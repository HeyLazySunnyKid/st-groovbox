# vim: syntax=spec

Name: {{{ git_dir_name }}}
Version: {{{ git_dir_version }}}
Release: 7%{?dist}
Summary: simple terminal emulator for X
License: MIT
URL: https://dl.suckless.org
VCS: {{{ git_dir_vcs }}}
Source: {{{ git_dir_pack }}}

BuildRequires:  ncurses-devel
BuildRequires:  libXext-devel
Requires:       libXft

%description
simple terminal emulator for X

%prep
{{{ git_dir_setup_macro }}}

%build
make

%install
make --directory=%{buildroot} install 

%files
/usr/bin/st

%changelog
{{{ git_dir_changelog }}}
